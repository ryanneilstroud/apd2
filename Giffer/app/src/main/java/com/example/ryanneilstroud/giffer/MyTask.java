package com.example.ryanneilstroud.giffer;

import android.os.AsyncTask;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by RyanStroud on 21/1/15.
 */
class MyTask extends AsyncTask<URL, Integer, GiphyUserResults> {

    @Override
    protected GiphyUserResults doInBackground(URL... urls) {

        String jsonString = null;

        for(URL queryURL : urls) {
            URLConnection conn;
            try {
                conn = queryURL.openConnection();
                jsonString = IOUtils.toString(conn.getInputStream());

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        JSONObject apiData = null;

        try {
            apiData = new JSONObject(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String data = null;
        GiphyUserResults results = new GiphyUserResults();

        try {
            for (int i = 0; i < apiData.getJSONArray("data").length(); i++) {
                data = apiData.getJSONArray("data").getJSONObject(i).getJSONObject("images").getJSONObject("fixed_width").getString("url");

                results.putUrl(data);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return results;
    }
}