package com.example.ryanneilstroud.giffer;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by RyanStroud on 30/1/15.
 */
public class GifFrag extends Fragment {

    static boolean aBoolean;

    public static GifFrag newInstance(boolean b) {
        GifFrag frag = new GifFrag();

        aBoolean = b;

        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater _inflater, ViewGroup _container,
                             Bundle _savedInstanceState) {
        if(aBoolean == true){
            // Create and return view for this fragment.
            View view = _inflater.inflate(R.layout.search, _container, false);
            return view;
        } else {
            // Create and return view for this fragment.
            View view = _inflater.inflate(R.layout.home_fragment, _container, false);
            return view;
        }

    }

    @Override
    public void onActivityCreated(Bundle _savedInstanceState) {
        super.onActivityCreated(_savedInstanceState);

    }
}
