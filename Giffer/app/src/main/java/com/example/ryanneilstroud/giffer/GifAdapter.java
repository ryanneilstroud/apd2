package com.example.ryanneilstroud.giffer;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.GridView;

/**
 * Created by RyanStroud on 21/1/15.
 */
public class GifAdapter extends BaseAdapter {

    private Context mContext;
    private GiphyUserResults mResults;

    public GifAdapter(Context c, GiphyUserResults results) {
        mContext = c;
        mResults = results;
    }

    @Override
    public int getCount() {
        return mResults.getUrlArr().size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        WebView webView;
        if (convertView == null) {
            webView = new WebView(mContext);
            webView.setLayoutParams(new GridView.LayoutParams(450, 450));
            webView.setPadding(15, 15, 15, 15);

        } else {
            webView = (WebView) convertView;
        }

        webView.loadUrl(mResults.getUrlArr().get(position));
        webView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                shareImage(position);

                return false;
            }
        });

        return webView;
    }

    private void shareImage(int position) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, mResults.getUrlArr().get(position));
        mContext.startActivity(Intent.createChooser(intent, "Share"));
    }
}