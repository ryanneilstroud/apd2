package com.example.ryanneilstroud.giffer;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.SearchView;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

/**
 * Created by RyanStroud on 20/1/15.
 */
public class GifPicker extends Activity implements SearchView.OnQueryTextListener {

    String urlString;
    String mQuery = "";
    GiphyUserResults results = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search);

        getURL();

        searchGiphy(urlString);

        setGridView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my, menu);
        SearchView mSearchView = (SearchView) menu.findItem(R.id.my_action_search).getActionView();
        mSearchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Store our shared preference
        SharedPreferences sp = getSharedPreferences("OURINFO", MODE_PRIVATE);
        Editor ed = sp.edit();
        ed.putBoolean("active", true);
        ed.commit();
    }

    @Override
    protected void onStop() {
        super.onStop();

        // Store our shared preference
        SharedPreferences sp = getSharedPreferences("OURINFO", MODE_PRIVATE);
        Editor ed = sp.edit();
        ed.putBoolean("active", false);
        ed.commit();
    }

    private void setGridView() {
        GridView gridview = (GridView)findViewById(R.id.gridView);
        gridview.setAdapter(new GifAdapter(this, results));
    }

    public void getURL(){
        urlString = null;

        if (mQuery.matches("")) {
            System.out.println("EQUALS NOTHING");
            urlString = "http://api.giphy.com/v1/gifs/trending?api_key=dc6zaTOxFJmzC";
        } else {
            System.out.println("EQUALS SOMETHING");
            urlString = "http://api.giphy.com/v1/gifs/search?q=" + mQuery +"&api_key=dc6zaTOxFJmzC";
            urlString = urlString.replaceAll(" ", "+");
        }
    }

    public void searchGiphy(String s){
        URL url = null;

        try {
            url = new URL(s);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        MyTask task = new MyTask();
        try {
            results = task.execute(url).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        mQuery = query;

        getURL();

        searchGiphy(urlString);

        setGridView();

        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }
}
