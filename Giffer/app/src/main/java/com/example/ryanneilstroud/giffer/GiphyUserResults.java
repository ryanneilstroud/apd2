package com.example.ryanneilstroud.giffer;

import java.util.ArrayList;

/**
 * Created by RyanStroud on 21/1/15.
 */
public class GiphyUserResults {

    ArrayList<String> mUrlArr = new ArrayList<String>();

    public void putUrl(String url){
        mUrlArr.add(url);
    }

    public ArrayList<String> getUrlArr(){
        return mUrlArr;
    }
}

