package com.example.ryanneilstroud.giffer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;

public class MyActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        getFragmentManager().beginTransaction()
                .replace(R.id.container, GifFrag.newInstance(false))
                .commit();
    }

    public void openSettings(View v){
        startActivity(new Intent(Settings.ACTION_INPUT_METHOD_SETTINGS));
    }
}
