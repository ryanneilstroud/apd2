Course: Application Deployment II: Android
Term: 1501
Lecturer: Chad Gibson

Developer: 
	Ryan Stroud
Application Version Number: 
	3
Repository:
	https://bitbucket.org/ryanneilstroud/apd2
Change Log:
	⁃	Viewing gifs has moved from the keyboard to a new activity for efficiency.
Current Feature List:
	⁃	View trending gifs
	⁃	View searched gifs
	-       Share Gif